using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScenePauser : MonoBehaviour
{
    private Animator myAnimator;
    [SerializeField]
    private GameObject myPauseButton;
    [SerializeField]
    private GameObject myTextPaused;
    [SerializeField]
    private GameObject myImagePaused;
    [SerializeField]
    private GameObject mainMenuButton;
    private bool isPaused = false;

    // Start is called before the first frame update
    void Start()
    {
        myAnimator = gameObject.GetComponent<Animator>();
    }

    public void onClick()
    {
        if (!isPaused)
        {
            PauseGame();
            myAnimator.enabled = true;
            myAnimator.Play("");
            myAnimator.Play("PauseFadeIn");
            myPauseButton.transform.GetChild(0).GetComponent<Text>().text = "RESUME";
            isPaused = true;
        }
        else
        {
            myAnimator.enabled = false;
            myPauseButton.transform.GetChild(0).GetComponent<Text>().text = "PAUSE";
            myTextPaused.GetComponent<Text>().color = Color.clear;
            myImagePaused.GetComponent<Image>().color = Color.clear;
            mainMenuButton.GetComponent<Image>().color = Color.clear;
            mainMenuButton.GetComponent<Button>().interactable = false;
            mainMenuButton.transform.GetChild(0).GetComponent<Text>().color = Color.clear;
            isPaused = false;
            ResumeGame();
        }
    }

    void PauseGame()
    {
        Time.timeScale = 0;
    }

    void ResumeGame()
    {
        Time.timeScale = 1;
    }

    public void onClickMainScene()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("MainScene", LoadSceneMode.Single);
    }

}
