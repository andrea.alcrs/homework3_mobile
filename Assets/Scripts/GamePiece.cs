using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePiece : MonoBehaviour
{
    private int x;
    public int X { get { return x; }
        set {
            if (isMovable()) {
                x = value;
            }
        }
    }

    private int y;
    public int Y { get { return y; }
        set
        {
            if (isMovable())
            {
                y = value;
            }
        }
    }

    public static bool blocked = false;

    private GridController grid;

    public GridController gridRef { get { return grid; } }

    private GridController.PieceType type;

    public GridController.PieceType Type { get { return type; } }

    private MovablePiece movablePiece;

    public MovablePiece MovablePiece { get { return movablePiece; } }

    private ColoredPiece colorPiece;

    public ColoredPiece ColorPiece { get { return colorPiece; } }


    private ClearablePiece clearablePiece;

    public ClearablePiece ClearablePiece { get { return clearablePiece; } }

    public int score;

    private void Awake()
    {
        movablePiece = GetComponent<MovablePiece>();
        colorPiece = GetComponent<ColoredPiece>();
        clearablePiece = GetComponent<ClearablePiece>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!blocked)
        {
            for (int i = 0; i < Input.touchCount; ++i)
            {
                if (Input.GetTouch(i).phase == TouchPhase.Began)
                {
                    // Construct a ray from the current touch coordinates
                    Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(i).position);

                    // Create a particle if hit
                    if (Physics.Raycast(ray))
                    {
                        grid.TouchedPiece(this);
                    }
                }
                if (Input.GetTouch(i).phase == TouchPhase.Stationary)
                {
                    // Construct a ray from the current touch coordinates
                    Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(i).position);

                    // Create a particle if hit
                    if (Physics.Raycast(ray))
                    {
                        grid.ToBeSwappedPiece(this);
                        grid.ReleasePiece();
                    }
                }
                //if (Input.GetTouch(i).phase == TouchPhase.Ended)
                //{
                //        grid.ReleasePiece();
                //}
            }
        }
    }

    public void Init(int _x,int _y, GridController _grid,GridController.PieceType _type) {
        x = _x;
        y = _y;
        grid = _grid;
        type = _type;
    }
    private void OnMouseEnter()
    {
        if (!blocked)
        {
            grid.ToBeSwappedPiece(this);
            grid.ReleasePiece();
        }
    }

    private void OnMouseDown()
    {
        if(!blocked)
            grid.TouchedPiece(this);
    }

    //private void OnMouseUp()
    //{
    //    grid.ReleasePiece();   
    //}

    public bool isMovable() {
        return movablePiece != null;
    }

    public bool isColored()
    {
        return colorPiece != null;
    }

    public bool isClearable()
    {
        return clearablePiece != null;
    }
}
