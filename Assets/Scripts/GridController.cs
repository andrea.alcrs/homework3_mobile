using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridController : MonoBehaviour{
    //enum con le tipologie di pezzi nella griglia e count
    public enum PieceType { 
        NORMAL,
        COUNT,
    }
    //Dimensioni della griglia configurabili
    public int xDimension;
    public int yDimension;
    public float fillTime;
    //Struct con i tipi di pezzi e il rispettivo prefab
    [System.Serializable] public struct PiecePrefab {
        public PieceType pieceType;
        public GameObject prefab;
    }
    //Array dei prefab possibili da inserire nella griglia
    public PiecePrefab[] piecePrefabs;
    //Tile di sfondo della griglia
    public GameObject prefabBackGround;
    public Level level;
    //Dizionario mappato a partire da piecePrefabs usato per popolare l'array di pieces con le tipologie di pezzi da mettere sulla griglia
    private Dictionary<PieceType, GameObject> piecesDict;
    //Pezzi istanzaiati sulla griglia
    private GamePiece[,] pieces;
    private GamePiece touchedPiece;
    private GamePiece toSwapPiece;
    private bool gameOver=false;

    private int comboMultiplier = 1;

    // Start is called before the first frame update
    void Start()
    {
        //Aggiungo al dizionario i pezzi impostati dall'editor sull'array dei possibili pezzi in game (univoci per tipologia)
        piecesDict = new Dictionary<PieceType, GameObject>();
        for (int i = 0; i < piecePrefabs.Length; i++)
        {
            if (!piecesDict.ContainsKey(piecePrefabs[i].pieceType))
            {
                piecesDict.Add(piecePrefabs[i].pieceType, piecePrefabs[i].prefab);
            }
        }

        //Instanzio i tile di sfondo dei pezzi per ogni cella della griglia
        for (int x = 0; x < xDimension; x++)
        {
            for (int y = 0; y < yDimension; y++)
            {
                GameObject bg = (GameObject)Instantiate(prefabBackGround, GetWorldPosition(x, y), Quaternion.identity);
                bg.transform.parent = transform;
            }
        }

        pieces = new GamePiece[xDimension, yDimension];
        StartCoroutine(Fill());
    }

    //       X Y  
    //      |0,0|1,0|   |   |   |   |   |   |  
    //      |0,1|   |   |   |   |   |   |   |
    //      |   |   |   |   |   |   |   |   |
    //      |   |   |   |   |   |   |   |   |
    //      |   |   |   |   |   |   |   |   |
    //      |   |   |   |   |   |   |   |   |
    //      |   |   |   |   |   |   |   |   |
    //      |   |   |   |   |   |   |   |   |
    //
    public bool FillStep() {
        bool movedPiece = false;

            for (int y = yDimension -1; y >0;  y--)
            {
                for (int x = 0; x < xDimension; x++)
                {
                    GamePiece piece = pieces[x, y];
                    if (piece == null)
                    {
                        int yCycle = y - 1;
                        while (yCycle >= 0 && pieces[x, yCycle] == null)
                            yCycle--;

                        if (yCycle >= 0) {
                            pieces[x,yCycle].MovablePiece.Move(x, y, fillTime);
                            pieces[x, y] = pieces[x, yCycle];

                            pieces[x, yCycle] = null;

                            movedPiece = true;
                        }
                    }
                }
            }
        
            for (int y = yDimension - 1; y >= 0; y--)
            {
                for (int x = 0; x < xDimension; x++)
                {
                    if (pieces[x, y] == null)
                    {

                        GameObject newPiece = (GameObject)Instantiate(piecesDict[PieceType.NORMAL], GetWorldPosition(x, y - yDimension), Quaternion.identity);
                        newPiece.transform.parent = transform;

                        pieces[x, y] = newPiece.GetComponent<GamePiece>();
                        pieces[x, y].Init(x, y, this, PieceType.NORMAL);
                        pieces[x, y].MovablePiece.Move(x, y, fillTime);
                        pieces[x, y].ColorPiece.SetColor((ColoredPiece.ColorType)Random.Range(0, pieces[x, y].ColorPiece.NumColor));
                        movedPiece = true;
                    }
                }



            }

        return movedPiece;
    }

    public Vector2 GetWorldPosition(int x, int y) {
        return new Vector2(transform.position.x - xDimension / 2.0f + x, transform.position.y + yDimension / 2.0f - y);
    }
    public GamePiece SpawnNewPiece(int x, int y, PieceType type) {
        GameObject newPiece = (GameObject)Instantiate(piecesDict[type],GetWorldPosition(x,y), Quaternion.identity);
        newPiece.transform.parent = transform;

        pieces[x, y] = newPiece.GetComponent<GamePiece>();
        pieces[x, y].Init(x, y, this, type);
        return pieces[x, y];
    }
    // Update is called once per frame

    public bool IsAdjacent(GamePiece a, GamePiece b) {
        return (a.X == b.X && Mathf.Abs(a.Y - b.Y) == 1) || (a.Y == b.Y && Mathf.Abs(a.X - b.X) == 1);  
    }

    public void SwapPieces(GamePiece a, GamePiece b)
    {
        if (gameOver) {
            return;
        }
        if (a.isMovable() && b.isMovable()) {
            pieces[a.X, a.Y] = b;
            pieces[b.X, b.Y] = a;
            if (GetMatch(a, b.X, b.Y) != null || GetMatch(b, a.X, a.Y) != null)
            {
                GamePiece.blocked = true;
                int oldAX = a.X;
                int oldAY = a.Y;
                a.MovablePiece.Move(b.X, b.Y, fillTime);
                b.MovablePiece.Move(oldAX, oldAY, fillTime);
                ClearAllValidMatches();
                StartCoroutine(Fill());
                comboMultiplier++;
                level.OnMove();
            }
            else {
                pieces[a.X, a.Y] = a;
                pieces[b.X, b.Y] = b;
            }

        }
    }

    public IEnumerator Fill()
    {
        bool needsRefill = true;
        while (needsRefill)
        {
            yield return new WaitForSeconds(fillTime);

            while (FillStep())
            {
                yield return new WaitForSeconds(fillTime);
            }
            
            needsRefill = ClearAllValidMatches();
            comboMultiplier++;
        }

        if (!thereAreOtherPossibleMatchs())
        {
            GameOver();
            level.GameLose();
        }

        comboMultiplier = 1;

        GamePiece.blocked = false;

    }

    public void TouchedPiece(GamePiece piece) {
        touchedPiece = piece;
    }
    public void ToBeSwappedPiece(GamePiece piece)
    {
        toSwapPiece = piece;
    }
    public void ReleasePiece() {
        if (touchedPiece != null)
        {
            if (IsAdjacent(touchedPiece, toSwapPiece))
            {
                SwapPieces(touchedPiece, toSwapPiece);
                touchedPiece = null;
            }
        }
    }

    public List<GamePiece> GetMatch(GamePiece piece, int newX, int newY) {
        if (piece.isColored()) {
            ColoredPiece.ColorType color = piece.ColorPiece.Color;
            List<GamePiece> horizontalpieces = new List<GamePiece>();
            List<GamePiece> verticalpieces = new List<GamePiece>();
            List<GamePiece> matchingpieces = new List<GamePiece>();
            
            horizontalpieces.Add(piece);
            for (int dir = 0; dir <= 1; dir++) {
                for (int xOffset = 1; xOffset < xDimension; xOffset++) {
                    int x;
                    if (dir == 0) {// Sinistra
                        x = newX - xOffset;
                    } else {//Destra
                        x = newX + xOffset;
                    }
                    if (x < 0 || x >= xDimension) {
                        break;
                    }
                    if (pieces[x, newY] != null && pieces[x, newY].isColored() && pieces[x, newY].ColorPiece.Color == color)
                    {
                        horizontalpieces.Add(pieces[x, newY]);
                    }
                    else {
                        break;
                    }
                }
            }
            if (horizontalpieces.Count >= 3) {
                for (int i = 0; i < horizontalpieces.Count; i++) {
                    matchingpieces.Add(horizontalpieces[i]);
                }
            }
            //veriico i match a T e a L
            if (horizontalpieces.Count >= 3)
            {
                for (int i = 0; i < horizontalpieces.Count; i++)
                {
                    for (int dir = 0; dir <= 1; dir++)
                    {
                        for (int yOffset = 1; yOffset < yDimension; yOffset++)
                        {
                            int y;
                            if (dir == 0)
                            {// SU
                                y = newY - yOffset;
                            }
                            else
                            {//Giu
                                y = newY + yOffset;
                            }
                            if (y < 0 || y >= yDimension)
                            {
                                break;
                            }
                            if (pieces[horizontalpieces[i].X, y]!= null && pieces[horizontalpieces[i].X, y].isColored() && pieces[horizontalpieces[i].X, y].ColorPiece.Color == color) {
                                verticalpieces.Add(pieces[horizontalpieces[i].X, y]);
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                    if (verticalpieces.Count < 2)
                    {
                        verticalpieces.Clear();
                    }
                    else {
                        for (int j = 0; j < verticalpieces.Count; j++) {
                            matchingpieces.Add(verticalpieces[j]);
                        }
                        break;
                    }
                }
            }
            if (matchingpieces.Count >= 3) {
                return matchingpieces;
            }

            horizontalpieces.Clear();
            verticalpieces.Clear();
            verticalpieces.Add(piece);
            for (int dir = 0; dir <= 1; dir++)
            {
                for (int yOffset = 1; yOffset < yDimension; yOffset++)
                {
                    int y;
                    if (dir == 0)
                    {// SU
                        y = newY - yOffset;
                    }
                    else
                    {//Giu
                        y = newY + yOffset;
                    }
                    if (y < 0 || y >= yDimension)
                    {
                        break;
                    }
                    if (pieces[newX, y] != null && pieces[newX, y].isColored() && pieces[newX, y].ColorPiece.Color == color)
                    {
                        verticalpieces.Add(pieces[newX, y]);
                    }
                    else
                    {
                        break;
                    }
                }
            }
            if (verticalpieces.Count >= 3)
            {
                for (int i = 0; i < verticalpieces.Count; i++)
                {
                    matchingpieces.Add(verticalpieces[i]);
                }
            }

            //veriico i match a T e a L
            if (verticalpieces.Count >= 3)
            {
                for (int i = 0; i < verticalpieces.Count; i++)
                {
                    for (int dir = 0; dir <= 1; dir++)
                    {
                        for (int xOffset = 1; xOffset < xDimension; xOffset++)
                        {
                            int x;
                            if (dir == 0)
                            {// Sinistra
                                x = newX - xOffset;
                            }
                            else
                            {//Destra
                                x = newX + xOffset;
                            }
                            if (x < 0 || x >= xDimension)
                            {
                                break;
                            }
                            if (pieces[x, verticalpieces[i].Y]  != null && pieces[x, verticalpieces[i].Y].isColored() && pieces[x, verticalpieces[i].Y].ColorPiece.Color == color)
                            {   
                                horizontalpieces.Add(pieces[x, verticalpieces[i].Y]);
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                    if (horizontalpieces.Count < 2)
                    {
                        horizontalpieces.Clear();
                    }
                    else
                    {
                        for (int j = 0; j < horizontalpieces.Count; j++)
                        {
                            matchingpieces.Add(horizontalpieces[j]);
                        }
                        break;
                    }
                }
            }

            if (matchingpieces.Count >= 3)
            {
                return matchingpieces;
            }
        }
        return null;
    }

    private bool thereAreOtherPossibleMatchs()
    {
        for (int y = 0; y < yDimension; y++)
        {
            for (int x = 0; x < xDimension; x++)
            {
                var piece = pieces[x, y];
                List<GamePiece> match = null;
                if ( x + 1 < xDimension)
                match = GetMatch(piece, x + 1, y);
                if (match != null)
                {
                    for (int i = 0; i < match.Count; i++)
                    {
                        if (ClearPiece(match[i].X, match[i].Y, true))
                        {
                            return true;
                        }
                    }
                }
                match?.Clear();
                if(x - 1 >= 0)
                match = GetMatch(piece, x - 1, y);
                if (match != null)
                {
                    for (int i = 0; i < match.Count; i++)
                    {
                        if (ClearPiece(match[i].X, match[i].Y, true))
                        {
                            return true;
                        }
                    }
                }
                match?.Clear();
                if(y + 1 < yDimension)
                match = GetMatch(piece, x, y + 1);
                if (match != null)
                {
                    for (int i = 0; i < match.Count; i++)
                    {
                        if (ClearPiece(match[i].X, match[i].Y, true))
                        {
                            return true;
                        }
                    }
                }
                match?.Clear();
                if(y - 1 >=0)
                match = GetMatch(piece, x, y - 1);
                if (match != null)
                {
                    for (int i = 0; i < match.Count; i++)
                    {
                        if (ClearPiece(match[i].X, match[i].Y, true))
                        {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public bool ClearAllValidMatches() {
        bool needsRefill = false;
        for (int y = 0; y < yDimension; y++) {
            for (int x = 0; x < xDimension; x++) {
                if (pieces[x, y] != null && pieces[x, y].isClearable()) {
                    List<GamePiece> match = GetMatch(pieces[x, y], x, y);
                    if (match != null) {
                        for (int i = 0; i < match.Count; i++) {
                            if (ClearPiece(match[i].X, match[i].Y, false))
                            {
                                needsRefill = true;
                            }
                        }
                    }
                    
                }
            }
        }
        return needsRefill;
    }
    public bool ClearPiece(int x, int y, bool isACheck) {
        if (pieces[x, y]!= null && pieces[x, y].isClearable() && !pieces[x, y].ClearablePiece.IsBeingCleared) {
            if (!isACheck)
            {
                ColoredPiece currentPiece = pieces[x, y].ColorPiece;
                
                pieces[x, y].ClearablePiece.Clear(comboMultiplier);

                if (currentPiece.IsPowerUp)
                {
                    //freeze
                    if (currentPiece.PowerUpType == 0)
                    {
                        level.FreezePowerUp();
                    }
                    // BOMBS
                    else
                    {
                        for (int newY = -1; newY <= 1; newY++)
                        {

                            int newnewY = y + newY;

                            if (newnewY >= 0 && newnewY < yDimension)
                            {

                                for (int newX = -1; newX <= 1; newX++)
                                {
                                    int newnewX = x + newX;
                                    if (newnewX >= 0 && newnewX < xDimension)
                                    {
                                        if (newX != 0 || newY != 0)
                                        {
                                            ClearPiece(newnewX, newnewY, false);
                                        }//else stack overflow
                                    }
                                }
                            }
                        }
                    }
                }

                pieces[x, y] = null;
            }
            return true;
        }
        return false;
    }
    public void GameOver() {
        gameOver = true;
    }
}
