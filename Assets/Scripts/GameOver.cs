using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameOver : MonoBehaviour
{
    [SerializeField] Button restartButton;
    [SerializeField] Button mainMenuButton;
    private Animator myAnimator;
    // Start is called before the first frame update
    void Start()
    {
        myAnimator = gameObject.GetComponent<Animator>();
    }

    public void StartGameOver() {
        restartButton.gameObject.SetActive(true);
        mainMenuButton.gameObject.SetActive(true);
        myAnimator.Play("GameOver");
    }

    public void Restart() {
        restartButton.gameObject.SetActive(false);
        mainMenuButton.gameObject.SetActive(false);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
    }

    public void MainMenu() {
        restartButton.gameObject.SetActive(false);
        mainMenuButton.gameObject.SetActive(false);
        SceneManager.LoadScene("MainScene",LoadSceneMode.Single);
    }
     
    // Update is called once per frame
    void Update()
    {
        
    }
}
