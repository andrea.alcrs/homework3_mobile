using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Level : MonoBehaviour
{
    public GridController grid;
    public GameOver gameOver;

    [SerializeField] Text scoreText;
    [SerializeField] Text timerText;
    [SerializeField] Text bestScoreText;
    [SerializeField] Canvas gameOverCanvas;
    [SerializeField] GameObject pauseCanvas;

    protected int currentScore;
    public int freezeTimeSeconds;
    public int timeInSeconds;
    public int TargetScore;
    private double timer;
    private bool timerEnded=false;
    public static string HighScoreKey = "HighScore";
    private int highScore;
    private Coroutine freezeCoRoutineRef;
    private bool freeze;
    private double cDown;
    // Start is called before the first frame update
    void Start()
    {
        highScore = PlayerPrefs.GetInt(HighScoreKey, 0);
        cDown = timeInSeconds;
    }

    // Update is called once per frame
    void Update()
    {
        if (!freeze)
        {
            timer = Time.deltaTime;
        }
        else {
            timer = 0;
        }
        
        cDown -= timer;
        if (cDown <= 0) {
            if (currentScore >= TargetScore)
            {
                GameWin();
            }
            else {
                GameLose();
            }
        }
        String minSec = string.Format("{0}:{1:00}", (int)cDown / 60, (int)cDown % 60);
        timerText.text = minSec;
    }
    public void FreezePowerUp() {
        if (freezeCoRoutineRef != null) {
            StopCoroutine(freezeCoRoutineRef);
            freezeCoRoutineRef = null;
        }

         freezeCoRoutineRef = StartCoroutine(freezeCoRoutine());
    }

    private IEnumerator freezeCoRoutine() {
        freeze = true;
        yield return new WaitForSeconds(freezeTimeSeconds);
        freeze = false;
    }
    public void GameWin() {
        Debug.Log("WIN");
        grid.GameOver();
    }
    public void GameLose()
    {
        timerEnded = true;
        grid.GameOver();
        pauseCanvas.SetActive(false);
        gameOver.StartGameOver();
    }
    public void OnMove()
    {
        Debug.Log("MOVED");
    }

    public void OnPieceCleared(GamePiece piece, int multiplier)
    {
        currentScore += piece.score * multiplier;
        if (currentScore > highScore) {
            highScore = currentScore;
            PlayerPrefs.SetInt(HighScoreKey, currentScore);
        }
        scoreText.text = currentScore.ToString();
        bestScoreText.text = highScore.ToString();
        Debug.Log("Score: " + currentScore);
    }

}
