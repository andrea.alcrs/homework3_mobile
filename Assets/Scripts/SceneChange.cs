using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class SceneChange : MonoBehaviour
{
    [SerializeField] Text powerUpText;
    [SerializeField] Text highScore;
    [SerializeField] Animator sceneTransactor;
    [SerializeField] AnimationClip transactionAnimation;

    private string powerUpKey = "PowerUp";
    private string powerUp;
    private void Start()
    {
        setPowerUp("PK FREEZE");
        highScore.text = PlayerPrefs.GetInt(Level.HighScoreKey, 0).ToString();
    }

    public void setPowerUp(string _powerUp) {
        powerUp = _powerUp;
        powerUpText.text = "Selected Power-UP: " + _powerUp;
        PlayerPrefs.SetString(powerUpKey,_powerUp);
    }
    public void ChangeScene() {
        if(sceneTransactor == null || transactionAnimation == null)
            SceneManager.LoadScene("GameScene");
        else
        {
            StartCoroutine(PlayTransaction());
        }
        
    }

    private IEnumerator PlayTransaction()
    {
        sceneTransactor.Play(transactionAnimation.name);
        yield return new WaitForSeconds(transactionAnimation.length);
        SceneManager.LoadScene("GameScene");
    }
}
