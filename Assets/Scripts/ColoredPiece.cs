using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColoredPiece : MonoBehaviour
{
    [SerializeField]
    private float powerUpProbability = 0.03f;

    private bool isPowerUp;

    public bool IsPowerUp
    {
        get
        {
            return isPowerUp;
        }
    }

    private int powerUpType;

    public int PowerUpType
    {
        get
        {
            return powerUpType;
        }
    }
    public enum ColorType { 
        FIRE,
        SUN,
        ROCK,
        MOON,
        WATER,
        LEAF,
        COUNT
    }
    [System.Serializable]

    public struct PowerUPColorSprite
    {
        public ColorType colorType;
        public Sprite colorSprite;
    }
    [System.Serializable]
    public struct ColorSprite {
        public ColorType colorType;
        public Sprite colorSprite;
    }

    public ColorSprite[] colorSprites;
    public ColorSprite[] bombSprites;
    public ColorSprite[] freezeSprites;
    private ColorType color;
    public ColorType Color{
        get { return color; }
        set { SetColor(value); }
    }
    public int NumColor
    {
        get { return colorSprites.Length; }
    }
    private SpriteRenderer sprite;
    private Dictionary<ColorType, Sprite> colorSpriteDict;
    private Dictionary<ColorType, Sprite> powrUPcolorSpriteDict;

    private void Awake()
    {
        sprite = transform.GetComponent<SpriteRenderer>();
        colorSpriteDict = new Dictionary<ColorType, Sprite>();
        for (int i = 0; i < colorSprites.Length; i++) {
            if (!colorSpriteDict.ContainsKey(colorSprites[i].colorType)) {
                colorSpriteDict.Add(colorSprites[i].colorType, colorSprites[i].colorSprite);
            }
        }
        ColorSprite[] powerUpSprites;
        if (PlayerPrefs.GetString("PowerUp") == ("PK FREEZE"))
        {
            powerUpType = 0;
            powerUpSprites = freezeSprites;
        }
        else
        {
            powerUpType = 1;
            powerUpSprites = bombSprites;
        }
        powrUPcolorSpriteDict = new Dictionary<ColorType, Sprite>();
        for (int i = 0; i < powerUpSprites.Length; i++)
        {
            if (!powrUPcolorSpriteDict.ContainsKey(powerUpSprites[i].colorType))
            {
                powrUPcolorSpriteDict.Add(powerUpSprites[i].colorType, powerUpSprites[i].colorSprite);
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetColor(ColorType newColor) {
        color = newColor;
        isPowerUp = (Random.Range(0.0f, 1.0f) <= powerUpProbability);
        if (!isPowerUp) {
            if (colorSpriteDict.ContainsKey(newColor)) {
                sprite.sprite = colorSpriteDict[newColor];
            }
        }
        else
        {
            if (colorSpriteDict.ContainsKey(newColor))
            {

                sprite.sprite = powrUPcolorSpriteDict[newColor];
            }                   
        }
    }
}
